DROP TABLE IF EXISTS seleccion;
DROP TABLE IF EXISTS formulario_sintoma_respuesta;
DROP TABLE IF EXISTS formulario;
DROP TABLE IF EXISTS sintoma;
DROP TABLE IF EXISTS respuesta;
DROP TABLE IF EXISTS encuesta;
DROP TABLE IF EXISTS tipo_pregunta;

CREATE TABLE formulario (
    id serial PRIMARY KEY,
    nombre varchar (150) NOT NULL,
    version_formulario integer NOT NULL,
    fecha date NOT NULL DEFAULT CURRENT_DATE,
    UNIQUE(nombre, version_formulario)
);

CREATE TABLE tipo_pregunta (
    id serial PRIMARY KEY,
    tipo varchar (150) NOT NULL UNIQUE
);

CREATE TABLE sintoma (
    id serial,
    tipo_pregunta_id integer REFERENCES tipo_pregunta(id),
    sintoma varchar (250) NOT NULL UNIQUE,
    descripcion varchar (250),
    PRIMARY KEY(id, tipo_pregunta_id)
);

CREATE TABLE respuesta (
    id serial PRIMARY KEY,
    respuesta varchar (250) NOT NULL UNIQUE
);

CREATE TABLE formulario_sintoma_respuesta (
    formulario_id integer REFERENCES formulario(id),
    respuesta_id integer REFERENCES respuesta(id),
    sintoma_id integer,
    tipo_pregunta_id integer,
    punteo integer NOT NULL DEFAULT 0 CHECK (punteo >= 0),
    FOREIGN KEY (sintoma_id, tipo_pregunta_id) REFERENCES sintoma(id, tipo_pregunta_id),
    PRIMARY KEY(formulario_id, respuesta_id, sintoma_id, tipo_pregunta_id)
);

CREATE TABLE encuesta (
    id serial PRIMARY KEY,
    fecha date NOT NULL DEFAULT CURRENT_DATE,
    latitud integer NOT NULL CHECK(latitud >= -90 AND latitud <= 90),
    longitud integer NOT NULL CHECK(longitud >= -180 AND longitud <= 180)
);

CREATE TABLE seleccion (
    encuesta_id integer REFERENCES encuesta(id),
    formulario_id integer,
    respuesta_id integer,
    sintoma_id integer,
    tipo_pregunta_id integer,
    FOREIGN KEY (formulario_id, respuesta_id, sintoma_id, tipo_pregunta_id) REFERENCES formulario_sintoma_respuesta(formulario_id, respuesta_id, sintoma_id, tipo_pregunta_id),
    PRIMARY KEY(encuesta_id, formulario_id, respuesta_id, sintoma_id, tipo_pregunta_id)
);

INSERT INTO formulario (nombre, version_formulario)
VALUES ('covid-19', 1);

INSERT INTO tipo_pregunta (tipo)
VALUES ('seleccion unica');

INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Estas teniendo tos?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Estas teniendo escalofrios?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿En este momento o en días previos has tenido diarrea?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Tienes dolor de garganta?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Estas teniendo dolor de cuerpo y malestar general?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Estas presentando dolores de cabeza?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Has tenido fiebre?', 'más de 37.8 grados');
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Has perdido el olfato?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Tienes dificultad para respirar?', 'como si no entrara aire al pecho');
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Tienes fatiga?', 'Deterioro de movimientos y ganas de hacer algo');
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Has viajado en los últimos 14 días?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Has viajado a un área afectada por COVID 19?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Has estado en contacto en alguna persona con COVID 19?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Tienes más mucosidad de lo normal?', NULL);
INSERT INTO sintoma (tipo_pregunta_id, sintoma, descripcion)
VALUES (1, '¿Has tenido estornudos?', NULL);

INSERT INTO respuesta (respuesta)
VALUES ('si');
INSERT INTO respuesta (respuesta)
VALUES ('no');

INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 1, 1,1);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 1, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 2, 1,1);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 2, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 3, 1,1);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 3, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 4, 1,1);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 4, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 5, 1,1);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 5, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 6, 1,1);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 6, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 7, 1,1);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 7, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 8, 1,1);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 8, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 9, 1,2);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 9, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 10, 1,2);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 10, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 11, 1,3);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 11, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 12, 1,3);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 12, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 13, 1,3);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 13, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 14, 1,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 14, 2,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 15, 1,0);
INSERT INTO formulario_sintoma_respuesta (formulario_id, tipo_pregunta_id, sintoma_id, respuesta_id, punteo)
VALUES (1, 1, 15, 2,0);