import React, { Component } from 'react';
import logo from './logo.svg';
//import { Redirect } from 'react-router-dom';
import axios from 'axios';

class Test extends Component {

    constructor(props) {
        super(props);
        this.state = {
            apiResponse: "",
            redirect: false
        };
    }

    componentDidMount() {
        
        axios.post('http://localhost:9000/test', {
            nombre: 'covid-19',
            version_formulario: 1
        }
        )
            .then((res) => {
                this.setState({ apiResponse: res.data });
                console.log(this.state.apiResponse)
            }).catch((error) => {
                console.log(error)
            });

    }


    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h2>En la última semana, ¿Has presentado los siguientes signos y/o síntomas?</h2>
                </div>
            </div>
        );
    }
}

export default Test;