import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import logo from './logo.svg';
import { Redirect } from 'react-router-dom';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      apiResponse: "",
      redirect: false
    };
  }

  onSubmit = () => {
    return <Redirect to="/covid19" />
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    })
  }

  componentDidMount() {
    axios.get('http://localhost:9000/')
      .then(res => {
        this.setState({ apiResponse: res.data });
      })
      .catch(function (error) {
        console.log(error);
      })
  }


  render() {
    if (this.state.redirect) {
      return <Redirect to='/covid19' />
    }
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Bienvenido al TEST RÁPIDO de COVID-19</h2>
        </div>
        <p className="App-intro">{this.state.apiResponse}</p>
        <button onClick={this.setRedirect}>SIGUIENTE</button>

      </div>
    );
  }
}


export default App;
