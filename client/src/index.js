import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Covid19 from './Covid19';
import Test from './Test';
import './index.css';
import { Route, BrowserRouter as Router  } from 'react-router-dom';

const routing = (
  <Router>
    <div>
      <Route exact path="/" component={App} />
      <Route exact path="/covid19" component={Covid19} />
      <Route path="/covid19/test" component={Test} />
    </div>
  </Router>
)

ReactDOM.render(routing,
  document.getElementById('root')
);
