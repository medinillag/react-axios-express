import React, { Component } from 'react';
import logo from './logo.svg';
import { Redirect } from 'react-router-dom';

class Covid19 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect : false
    };
  }

  onSubmit = () => {
    return <Redirect to="/covid19/test" />
  }

  setRedirect = () => {
    this.setState({
      redirect: true
    })
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to='/covid19/test' />
    }
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>¡Importante!</h2>
        </div>
        <p>Te recordamos que es solo una herramienta para apoyar al cuidado de tu salud</p>
        <b>EN NINGÚN MOMENTO</b> SE DEBE TOMAR LO SUGERIDO EN EL TEST COMO UN DIAGNÓSTICO
        <p/>
        <button onClick={this.setRedirect}>INICIAR TEST</button>
      </div>
    );
  }
}


export default Covid19;
